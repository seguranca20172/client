package seg20172.impl2.client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.fips.FipsDRBG;
import org.bouncycastle.crypto.util.BasicEntropySourceProvider;
import org.bouncycastle.jcajce.provider.BouncyCastleFipsProvider;

import seg20172.impl2.security.crypto.AESCipher;
import seg20172.impl2.security.crypto.RSACipher;
import seg20172.impl2.security.random.FixedRandom;
import seg20172.impl2.view.U;

public class Client {

	static {
		Security.addProvider(new BouncyCastleFipsProvider());
		CryptoServicesRegistrar.setSecureRandom(FipsDRBG.SHA512_HMAC.fromEntropySource(new BasicEntropySourceProvider(new SecureRandom(), true)).build(null, false));
	}

	public static void main(final String[] args)
		throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, UnknownHostException, IOException, DecoderException, InvalidKeyException,
		InvalidKeySpecException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, CertificateException, KeyStoreException {
		final Client client = new Client(45678);
		client.connect();
		client.shutdown();
	}

	private static final int PBKDF2_ITERATIONS = 1000;
	private static final int DERIVED_KEY_LENGTH = 128;

	private final int port;

	private final AESCipher aes;
	private final RSACipher rsa;

	private final SecretKeyFactory pbkdf2;

	private Socket server;
	private PrintStream out;
	private Scanner in;

	public Client(final int port) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, KeyStoreException, CertificateException, IOException {
		this.port = port;

		pbkdf2 = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");

		final SecureRandom random = new FixedRandom();
		aes = new AESCipher(random);
		rsa = new RSACipher(random);
	}

	private Key deriveKey(final char[] password) throws InvalidKeySpecException, NoSuchAlgorithmException {
		final SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		final byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return new SecretKeySpec(pbkdf2.generateSecret(new PBEKeySpec(
			password,
			salt,
			PBKDF2_ITERATIONS,
			DERIVED_KEY_LENGTH)).getEncoded(), "AES");
	}

	public void connect() throws UnknownHostException, IOException, DecoderException, InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException,
		InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, CertificateException, KeyStoreException {
		U.say("Tentando conectar-se a 127.0.0.1");
		server = new Socket("127.0.0.1", port);
		U.say("Conectado!");

		in = new Scanner(server.getInputStream());
		out = new PrintStream(server.getOutputStream());

		final String certificate = in.nextLine();
		U.say("Recebido certificado:\n%s", certificate);

		final Certificate cert = CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(Hex.decodeHex(certificate.toCharArray())));
		rsa.setPublicKey(cert.getPublicKey());

		final char[] pubKey = new String(rsa.getPublicKey().getEncoded()).toCharArray();
		final Key key = deriveKey(pubKey);
		aes.setKey(key);

		final char[] simmetricKey = Hex.encodeHex(rsa.encrypt(key.getEncoded()));
		out.println(simmetricKey);
		U.say("Enviada chave simetrica:\n%s", String.valueOf(simmetricKey));

		do {
			final String message = U.ask("Mensagem para o servidor");

			final char[] sent = Hex.encodeHex(aes.encrypt(message.getBytes()));
			U.say(
				"\n---------------------------------------------------------------------------------\nMENSAGEM ENVIADA\nMensagem texto plano\t>>>>\t%s\nMensagem codificada\t>>>>\t%s\n---------------------------------------------------------------------------------\n",
				message,
				String.valueOf(sent));

			out.println(sent);

			U.say("Aguardando resposta do servidor...");

			final String received = in.nextLine();
			final String decrypted = new String(aes.decrypt(Hex.decodeHex(received.toCharArray())));
			U.say(
				"\n---------------------------------------------------------------------------------\nMENSAGEM RECEBIDA\nMensagem texto plano\t>>>>\t%s\nMensagem codificada\t>>>>\t%s\n---------------------------------------------------------------------------------\n",
				decrypted,
				received);
		} while(true);
	}

	public void shutdown() throws IOException {
		server.close();
	}

}
